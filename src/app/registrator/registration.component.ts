import {Component, OnInit} from '@angular/core';
import {Patients} from '../shared/patients';
import {AuthService} from '../shared/services/auth.service';
import {StudyService} from '../shared/services/study.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  loader = false;
  patients: Patients[] = [];
  form: FormGroup;

  constructor(private authService: AuthService,
              private studyService: StudyService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl('')
    });
    this.loader = true;
    this.studyService.getStudies().subscribe(next => {
      this.patients = next;
      this.loader = false;
      console.log(this.patients);
    }, error => {
      console.log(error);
      this.loader = false;
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const patient = {
      firstname: this.form.value.firstName,
      lastname: this.form.value.lastName
    };

    this.studyService.addPatient(patient).subscribe(next => {
      console.log('next', next);
    })
  }
}
