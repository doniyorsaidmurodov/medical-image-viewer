import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StudyListComponent} from './study-list/study-list.component';
import {AuthComponent} from './auth/auth.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthInterceptor} from './shared/services/auth.interceptor';
import { PatientComponent } from './patient/patient.component';
import { AlertComponent } from './shared/alert/alert.component';
import {AlertService} from './shared/services/alert.service';
import { RegistrationComponent } from './registrator/registration.component';
import { RadiologyComponent } from './radiolog/radiology.component';

@NgModule({
  declarations: [
    AppComponent,
    StudyListComponent,
    AuthComponent,
    PatientComponent,
    AlertComponent,
    RegistrationComponent,
    RadiologyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
