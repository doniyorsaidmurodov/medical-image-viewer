export interface Patients {
  lastname: string;
  firstname: string;
  id: string;
  patient_birthdate: string;
  patient_gender: string;
  patient_id: string;
  patient_name: string;
}
