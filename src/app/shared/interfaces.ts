export interface User {
  username: string;
  password: string;
}

export interface FileSet {
  data: string;
  file: string;
  id: string;
  image: string;
  series: string;
}

export interface ToolData {
  Length?: Data;
  ArrowAnnotate?: Data;
  Angle?: Data;
  Probe?: Data;
  EllipticalRoi?: Data;
  RectangleRoi?: Data;
  Bidirectional?: Data;
}

export interface Data {
  data: [];
}

export interface SetDrawData {
  file_id: string;
  data?: string;
}


