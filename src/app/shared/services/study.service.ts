import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudyService {

  url: string = 'https://packs.polito.uz/api/v1/dicom';
  // url: string = 'http://127.0.0.1:8000/api/v1/dicom';

  constructor(private http: HttpClient) {
  }

  getStudies(): Observable<any> {
    return this.http.get(this.url + '/patients/');
  }

  getStudyById(id: string): Observable<any> {
    return this.http.get(this.url + `/patients/${id}/`);
  }

  saveData(data): Observable<any> {
    return this.http.post(this.url + `/set-draw-data/`, data);
  }

  getData(): Observable<any> {
    return this.http.get( this.url + '/draw-data/');
  }

  addPatient(patient): Observable<any> {
    return this.http.post(this.url + `/patients/`, patient)
  }


}
